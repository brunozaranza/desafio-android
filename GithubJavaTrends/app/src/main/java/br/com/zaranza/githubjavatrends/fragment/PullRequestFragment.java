package br.com.zaranza.githubjavatrends.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.toolbox.Volley;

import org.parceler.Parcels;

import java.util.List;

import br.com.zaranza.githubjavatrends.R;
import br.com.zaranza.githubjavatrends.activity.CollapsingToolbarActivity;
import br.com.zaranza.githubjavatrends.adapter.PullRequestAdapter;
import br.com.zaranza.githubjavatrends.databinding.FragmentPullRequestBinding;
import br.com.zaranza.githubjavatrends.model.PullRequest;
import br.com.zaranza.githubjavatrends.model.Repository;
import br.com.zaranza.githubjavatrends.service.PullRequestService;

/**
 * Created by mobileinbr on 13/01/17.
 */

public class PullRequestFragment extends Fragment {

    private FragmentPullRequestBinding binding;

    private Repository repository;

    private RecyclerView recyclerView;
    private List<PullRequest> pullRequests;
    private PullRequestAdapter pullRequestAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pull_request, container, false);

        View view = binding.getRoot();

        repository = Parcels.unwrap(getArguments().getParcelable(Repository.INTENT_KEY));

        loadDataFromApi(repository.owner.login, repository.name);

        if (repository != null) {

            CollapsingToolbarActivity activity = (CollapsingToolbarActivity) getActivity();
            activity.setAppBarImage(repository.bitmapImg);
            activity.setAppBarTitle(repository.name);
        }

        setupPullToRefresh();
        setupRecyclerView();

        return view;
    }

    private void setupPullToRefresh(){
        binding.pullToRefresh.hasNestedScrollingParent();
        binding.pullToRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        loadDataFromApi(repository.owner.login, repository.name);
                    }
                }
        );
    }

    private void setupRecyclerView(){
        recyclerView = binding.prRecyclerView;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void loadDataFromApi(String ownerName, String repositoryName){

        PullRequestService.getPullRequests(getContext(),ownerName, repositoryName, new PullRequestService.Callback() {
            @Override
            public void onResponse(List<PullRequest> response) {

                if (response == null) return;

                binding.txtOpened.setText(response.size()+" opened");
                binding.txtClosed.setText("");

                binding.progressBar.setVisibility(View.GONE);
                binding.pullToRefresh.setRefreshing(false);

                if (pullRequests == null) pullRequests = response;
                else for (PullRequest pullRequest : response) pullRequests.add(pullRequest);

                if (pullRequestAdapter == null){
                    pullRequestAdapter = new PullRequestAdapter(getContext(), pullRequests, onClickPullRequest());
                    recyclerView.setAdapter(pullRequestAdapter);
                } else {
                    pullRequestAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError() {
            }
        });
    }

    private PullRequestAdapter.OnClickListener onClickPullRequest() {

        return new PullRequestAdapter.OnClickListener() {
            @Override
            public void onClickRepository(PullRequestAdapter.PullRequestViewHolder holder, int idx) {

                PullRequest pullRequest = pullRequests.get(idx);

                String url = pullRequest.html_url;

                Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
                startActivity(intent);
            }
        };
    }
}
