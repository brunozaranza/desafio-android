package br.com.zaranza.githubjavatrends.service;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.zaranza.githubjavatrends.util.API;
import br.com.zaranza.githubjavatrends.model.Repository;

/**
 * Created by brunozaranza on 12/01/17.
 */

public class RepositoryService {

    public interface Callback {
        void onResponse(List<Repository> list);
        void onError();
    }

    public static synchronized void getRepositories(Context context, int page, final Callback callback) {

        String url = API.ENDPOINT+API.SEARCH_URL+page;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject object) {

                        try {
                            callback.onResponse(RepositoryService.setValues(object.getJSONArray("items")));
                        } catch (JSONException e) {
                            callback.onError();
                        }
                    }
                },
                new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                Log.e("Volley", "Error");
                callback.onError();
            }
        });

        Volley.newRequestQueue(context).add(request);
    }

    private static synchronized List<Repository> setValues(JSONArray array){

        List<Repository> repositories = new ArrayList<>(array.length());

        try {
            for (int i = 0; i < array.length(); i++){
                Gson gson = new Gson();
                Repository repository = gson.fromJson(array.getJSONObject(i).toString(), Repository.class);

                repositories.add(repository);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repositories;
    }
}
