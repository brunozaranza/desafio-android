package br.com.zaranza.githubjavatrends.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.List;

import br.com.zaranza.githubjavatrends.R;
import br.com.zaranza.githubjavatrends.helper.DateHelper;
import br.com.zaranza.githubjavatrends.helper.ImageHelper;
import br.com.zaranza.githubjavatrends.model.Repository;

/**
 * Created by brunozaranza on 12/01/17.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.RepositoryViewHolder> {

    private final List<Repository> repositories;
    private final Context context;
    private final OnClickListener onClickListener;

    public interface OnClickListener {
        public void onClickRepository(RepositoryViewHolder holder, int idx);
    }

    public RepositoryAdapter(Context context, List<Repository> repositories, OnClickListener onClickListener) {
        this.context = context;
        this.repositories = repositories;
        this.onClickListener = onClickListener;
    }

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.card_repository, viewGroup, false);
        RepositoryViewHolder holder = new RepositoryViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final RepositoryViewHolder holder, final int position) {
        final Repository repository = repositories.get(position);
        holder.name.setText(repository.name);
        holder.description.setText(repository.description);
        holder.forks.setText(String.valueOf(repository.forks));
        holder.stars.setText(String.valueOf(repository.stargazers_count));

        holder.username.setText(repository.owner.login);
        holder.fullname.setText(repository.full_name);

        holder.update.setText(DateHelper.getLastUpdate(repository.updated_at));
        ImageHelper.getBitmap(context, Uri.parse(repository.owner.avatar_url), 300, 300, false, new ImageHelper.Callback() {
            @Override
            public void onResponse(Bitmap bitmap) {
                repository.bitmapImg = bitmap;
                holder.img.setImageBitmap(bitmap);
            }

            @Override
            public void onError() {

            }
        });


        if (onClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClickRepository(holder, position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return this.repositories != null ? this.repositories.size() : 0;
    }

    public static class RepositoryViewHolder extends RecyclerView.ViewHolder {

        public ImageView img;

        public TextView name;
        public TextView description;
        public TextView forks;
        public TextView stars;
        public TextView username;
        public TextView fullname;
        public TextView update;
        public View view;

        public RepositoryViewHolder(View view) {
            super(view);
            this.view = view;

            img = (ImageView) view.findViewById(R.id.img);

            name = (TextView) view.findViewById(R.id.title);
            description = (TextView) view.findViewById(R.id.description);
            forks = (TextView) view.findViewById(R.id.txtForks);
            stars = (TextView) view.findViewById(R.id.txtStars);
            username = (TextView) view.findViewById(R.id.username);
            fullname = (TextView) view.findViewById(R.id.fullname);
            update = (TextView) view.findViewById(R.id.update);
//            progress = (ProgressBar) view.findViewById(R.id.progress);
        }
    }
}
