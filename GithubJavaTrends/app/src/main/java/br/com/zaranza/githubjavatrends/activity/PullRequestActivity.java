package br.com.zaranza.githubjavatrends.activity;

import android.os.Bundle;
import android.view.MenuItem;

import br.com.zaranza.githubjavatrends.R;
import br.com.zaranza.githubjavatrends.fragment.PullRequestFragment;

/**
 * Created by mobileinbr on 13/01/17.
 */

public class PullRequestActivity  extends CollapsingToolbarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pull_request);

        initViews();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(savedInstanceState == null) {
            PullRequestFragment frag = new PullRequestFragment();
            frag.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction().add(R.id.fragContainer, frag, null).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
