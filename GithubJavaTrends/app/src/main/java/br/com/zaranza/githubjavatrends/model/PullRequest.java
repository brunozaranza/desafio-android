package br.com.zaranza.githubjavatrends.model;

/**
 * Created by brunozaranza on 13/01/17.
 */

public class PullRequest {

    public int number;
    public String title;
    public String body;
    public String html_url;
    public String created_at;
    public String updated_at;
    public User user;
    public Milestone milestone;

}
