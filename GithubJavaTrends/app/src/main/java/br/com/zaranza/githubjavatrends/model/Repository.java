package br.com.zaranza.githubjavatrends.model;

import android.graphics.Bitmap;

import org.parceler.Parcel;

/**
 * Created by brunozaranza on 12/01/17.
 */

@Parcel
public class Repository {

    public static final String INTENT_KEY = "Repository";

    public int id;
    public String name;
    public String full_name;
    public Owner owner;
    public String html_url;
    public String description;
    public boolean fork;
    public String url;
    public String created_at;
    public String updated_at;
    public String pushed_at;
    public String git_url;
    public String ssh_url;
    public String clone_url;
    public String svn_url;
    public int size;
    public int stargazers_count;
    public int watchers_count;
    public String language;
    public int forks_count;
    public int open_issues_count;
    public int forks;
    public int open_issues;
    public int watchers;
    public float score;

    public Bitmap bitmapImg;
}
