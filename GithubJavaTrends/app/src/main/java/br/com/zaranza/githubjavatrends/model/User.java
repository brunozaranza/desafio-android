package br.com.zaranza.githubjavatrends.model;

/**
 * Created by brunozaranza on 13/01/17.
 */

public class User {

    public int id;
    public String login;
    public String avatar_url;
}
