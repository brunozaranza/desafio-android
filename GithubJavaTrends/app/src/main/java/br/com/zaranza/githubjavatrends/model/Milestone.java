package br.com.zaranza.githubjavatrends.model;

/**
 * Created by brunozaranza on 14/01/17.
 */

public class Milestone {

    public String open_issues;
    public String closed_issues;
}
