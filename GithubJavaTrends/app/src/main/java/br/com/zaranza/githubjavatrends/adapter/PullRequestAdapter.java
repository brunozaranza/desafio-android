package br.com.zaranza.githubjavatrends.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.zaranza.githubjavatrends.R;
import br.com.zaranza.githubjavatrends.helper.DateHelper;
import br.com.zaranza.githubjavatrends.helper.ImageHelper;
import br.com.zaranza.githubjavatrends.model.PullRequest;

/**
 * Created by brunozaranza on 13/01/17.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.PullRequestViewHolder> {

    private final List<PullRequest> pullRequests;
    private final Context context;
    private final PullRequestAdapter.OnClickListener onClickListener;

    public interface OnClickListener {
        public void onClickRepository(PullRequestViewHolder holder, int idx);
    }

    public PullRequestAdapter(Context context, List<PullRequest> pullRequests, PullRequestAdapter.OnClickListener onClickListener) {
        this.context = context;
        this.pullRequests = pullRequests;
        this.onClickListener = onClickListener;
    }

    @Override
    public  PullRequestAdapter.PullRequestViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.card_pull_request, viewGroup, false);

        PullRequestAdapter.PullRequestViewHolder holder = new PullRequestAdapter.PullRequestViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final PullRequestAdapter.PullRequestViewHolder holder, final int position) {
        final PullRequest pullRequest = pullRequests.get(position);

        holder.title.setText(pullRequest.title);
        holder.description.setText(pullRequest.body);

        holder.username.setText(pullRequest.user.login);

        holder.update.setText(DateHelper.getLastUpdate(pullRequest.updated_at));

        ImageHelper.getBitmap(context, Uri.parse(pullRequest.user.avatar_url), 40, 40, true, new ImageHelper.Callback() {
            @Override
            public void onResponse(Bitmap bitmap) {
                holder.img.setImageBitmap(bitmap);
            }

            @Override
            public void onError() {

            }
        });


        if (onClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClickRepository(holder, position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return this.pullRequests != null ? this.pullRequests.size() : 0;
    }

    public static class PullRequestViewHolder extends RecyclerView.ViewHolder {

        public ImageView img;
        public TextView username;

        public TextView title;
        public TextView description;
        public TextView update;

        public View view;

        public PullRequestViewHolder(View view) {
            super(view);
            this.view = view;

            img = (ImageView) view.findViewById(R.id.img);
            username = (TextView) view.findViewById(R.id.username);

            title = (TextView) view.findViewById(R.id.title);
            description = (TextView) view.findViewById(R.id.description);
            update = (TextView) view.findViewById(R.id.update);
//            progress = (ProgressBar) view.findViewById(R.id.progress);
        }
    }
}