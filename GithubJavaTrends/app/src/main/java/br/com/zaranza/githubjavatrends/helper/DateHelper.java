package br.com.zaranza.githubjavatrends.helper;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by brunozaranza on 14/01/17.
 */

public class DateHelper {

    public static String getLastUpdate(String dateString) {
        String updateText;

        TimeZone tz = TimeZone.getTimeZone("UTC");
        Calendar cal = Calendar.getInstance(tz);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        sdf.setCalendar(cal);

        try {
            cal.setTime(sdf.parse(dateString));
            Date date = cal.getTime();
            Date dateToday = new Date();

            long diffInMs = dateToday.getTime() - date.getTime();

            long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);

            if (diffInSec < 60) {
                updateText = "Atualizado há " + diffInSec + (diffInSec == 1 ? " segundo":" segundos");
            } else if (diffInSec < 60*60) {
                updateText = "Atualizado há " + (int)(diffInSec/60) + ((int)(diffInSec/60) == 1 ? " minuto":" minutos");
            } else if (diffInSec < 24*60*60){
                updateText = "Atualizado há " + (int)((diffInSec/60)/60) + ((int)((diffInSec/60)/60) == 1 ? " hora":" horas");
            } else if (diffInSec < 30*24*60*60){
                updateText = "Atualizado há " + (int)(((diffInSec/60)/60)/24) + ((int)(((diffInSec/60)/60)/24)== 1 ? " dia":" dias");
            } else {
                sdf = new SimpleDateFormat("dd/MM/yyyy");
                updateText = "Atualizado dia " + sdf.format(date);
            }

        } catch (ParseException e){
            updateText = "Sem informação";
        }

        return updateText;
    }
}
