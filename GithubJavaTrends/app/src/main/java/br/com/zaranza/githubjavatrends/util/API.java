package br.com.zaranza.githubjavatrends.util;

/**
 * Created by mobileinbr on 13/01/17.
 */

public interface API {

    String TRANSITION_KEY = "transition_key";

    String ENDPOINT = "https://api.github.com";
    String SEARCH_URL = "/search/repositories?q=language:java&sort=stars&order=desc&page=";
    String PULL_REQUEST_URL = "/repos/";

}
