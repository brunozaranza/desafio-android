package br.com.zaranza.githubjavatrends.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.android.volley.toolbox.Volley;

import org.parceler.Parcels;

import java.util.List;

import br.com.zaranza.githubjavatrends.R;
import br.com.zaranza.githubjavatrends.adapter.RepositoryAdapter;
import br.com.zaranza.githubjavatrends.util.API;
import br.com.zaranza.githubjavatrends.databinding.ActivityMainBinding;
import br.com.zaranza.githubjavatrends.util.EndlessRecyclerViewScrollListener;
import br.com.zaranza.githubjavatrends.model.Repository;
import br.com.zaranza.githubjavatrends.service.RepositoryService;

public class RepositoryActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    private RecyclerView recyclerView;
    private List<Repository> repositories;
    private RepositoryAdapter repositoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setSupportActionBar(binding.toolbar);

        getSupportActionBar().setTitle("Github " + getResources().getString(R.string.app_name));

        setupFabButton();
        setupPullToRefresh();
        setupRecyclerView();

        loadNextDataFromApi(1);
    }

    private void setupFabButton() {

        binding.fabBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                recyclerView.getLayoutManager()
                        .scrollToPosition(0);
                binding.fabBottom.hide();
            }
        });
    }

    private void setupPullToRefresh(){

        binding.pullToRefresh.hasNestedScrollingParent();
        binding.pullToRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        repositories.clear();
                        repositories = null;
                        repositoryAdapter = null;
                        loadNextDataFromApi(1);
                    }
                }
        );
    }

    private void setupRecyclerView(){

        recyclerView = binding.recyclerView;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {
                if (velocityY < 0){
                    binding.fabBottom.show();
                }
                return false;
            }
        });
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page) {
                binding.progressBarLoadPage.setVisibility(View.VISIBLE);
                loadNextDataFromApi(page);
            }
        });

    }

    public void loadNextDataFromApi(int page) {

        RepositoryService.getRepositories(this, page, new RepositoryService.Callback() {
            @Override
            public void onResponse(List<Repository> response) {

                binding.pullToRefresh.setRefreshing(false);
                binding.progressBar.setVisibility(View.GONE);
                binding.progressBarLoadPage.setVisibility(View.GONE);

                if (repositories == null) repositories = response;
                else for (Repository repository : response) repositories.add(repository);

                if (repositoryAdapter == null){
                    repositoryAdapter = new RepositoryAdapter(getBaseContext(), repositories, onClickRepository());
                    recyclerView.setAdapter(repositoryAdapter);
                } else {
                    repositoryAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError() {

            }
        });
    }

    private RepositoryAdapter.OnClickListener onClickRepository() {

        return new RepositoryAdapter.OnClickListener() {
            @Override
            public void onClickRepository(RepositoryAdapter.RepositoryViewHolder holder, int idx) {

                openPullRequestActivity(repositories.get(idx), holder.img);
            }
        };
    }

    public void openPullRequestActivity(Repository repository, ImageView imgView){
        Intent intent = new Intent(this, PullRequestActivity.class);
        intent.putExtra(Repository.INTENT_KEY, Parcels.wrap(repository));

        ActivityOptionsCompat opts = ActivityOptionsCompat.makeSceneTransitionAnimation(this, imgView, API.TRANSITION_KEY);
        ActivityCompat.startActivity(this, intent, opts.toBundle());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
