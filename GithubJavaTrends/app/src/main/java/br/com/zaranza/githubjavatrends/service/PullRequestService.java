package br.com.zaranza.githubjavatrends.service;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import br.com.zaranza.githubjavatrends.util.API;
import br.com.zaranza.githubjavatrends.model.PullRequest;

/**
 * Created by mobileinbr on 13/01/17.
 */

public class PullRequestService {

    public interface Callback {
        void onResponse(List<PullRequest> list);
        void onError();
    }

    public static synchronized void getPullRequests(Context context, String ownerName, String repositoryName, final PullRequestService.Callback callback) {

        String url = API.ENDPOINT+API.PULL_REQUEST_URL+ownerName+"/"+repositoryName+"/pulls";

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray object) {
                        callback.onResponse(PullRequestService.setValues(object));
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){
                        Log.e("Volley", "Error");
                        callback.onError();
                    }
                }
        );

        Volley.newRequestQueue(context).add(request);
    }

    private static List<PullRequest> setValues(JSONArray array){

        List<PullRequest> pullRequests = new ArrayList<>(array.length());

        try {
            for (int i = 0; i < array.length(); i++){
                Gson gson = new Gson();

                PullRequest pullRequest = gson.fromJson(array.getJSONObject(i).toString(), PullRequest.class);

                pullRequests.add(pullRequest);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return pullRequests;
    }
}
