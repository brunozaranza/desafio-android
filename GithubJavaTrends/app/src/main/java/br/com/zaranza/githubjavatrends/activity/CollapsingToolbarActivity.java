package br.com.zaranza.githubjavatrends.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import br.com.zaranza.githubjavatrends.R;
import br.com.zaranza.githubjavatrends.util.API;

/**
 * Created by mobileinbr on 13/01/17.
 */

public class CollapsingToolbarActivity extends AppCompatActivity {


    private CollapsingToolbarLayout collapsingToolbar;

    private ImageView appBarImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pull_request);
    }

    public void initViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        appBarImg = (ImageView) findViewById(R.id.appBarImg);
        ViewCompat.setTransitionName(appBarImg, API.TRANSITION_KEY);
    }

    public void setAppBarTitle(String title) {
        collapsingToolbar.setTitle(title);
    }

    public void setAppBarImage(Bitmap bitmap) {
        appBarImg.setImageBitmap(bitmap);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}